﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FadeInText : MonoBehaviour
{
    [SerializeField] float LevelLoadDelay = 5f;
    public GameObject Text;
    Animator m_Animator;
    private void Start()
    {
        m_Animator = Text.GetComponent<Animator>();
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(0)) {  StartCoroutine(LoadNextLevel()); }

    }
    IEnumerator LoadNextLevel()
    {
        m_Animator.SetBool("exit", true);
        yield return new WaitForSecondsRealtime(LevelLoadDelay);
        var currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex + 1);


    }
}
