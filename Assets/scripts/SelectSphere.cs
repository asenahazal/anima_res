﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SelectSphere : MonoBehaviour
{
    [SerializeField] float LevelLoadDelay = 4f;
    public GameObject Sphere1;
    public GameObject Sphere2;

    public GameObject Spherelittle;

    Animator self_Animator;
    Animator m1_Animator;
    Animator m2_Animator;

    Vector3 startPos;
    Vector3 endPos;
    float lerpTime = 2f;
 

    bool selected;
    private void Start()
    {
        m1_Animator = Sphere1.GetComponent<Animator>();
        m2_Animator = Sphere2.GetComponent<Animator>();
        self_Animator = GetComponent<Animator>();
    }
    private void OnMouseDown()
    {

        Spherelittle.transform.parent = null;
        Destroy(GetComponent<Orbit>());
        m1_Animator.SetBool("exit", true);
        self_Animator.SetBool("exit", false);
        m2_Animator.SetBool("exit", true);

        selected = true;
        StartCoroutine(LoadNextLevel());
    }
    IEnumerator LoadNextLevel()
    {
        DontDestroyOnLoad(gameObject);
        yield return new WaitForSecondsRealtime(LevelLoadDelay);
        var currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex + 1);


    }
    // Update is called once per frame
    void Update()
    {
        Scene scene = SceneManager.GetActiveScene();
        if (scene.name == "1")
        { Destroy(gameObject); }
            startPos = this.transform.position;
        endPos = new Vector3(0, 0, 0);
        if (selected)
        {
            Debug.Log("entered");

            this.transform.position = Vector3.Lerp(startPos, endPos, lerpTime*Time.deltaTime);
        }

    }
}
