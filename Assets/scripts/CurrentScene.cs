﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CurrentScene : MonoBehaviour
{
    Animator m1_Animator;
    Animator m2_Animator;
    Animator m3_Animator;
    public Image Level1;
    public Image level2;
    public Image level3;
    public GameObject timeline;

    // Start is called before the first frame update
    void Start()
    {

        m1_Animator = Level1.GetComponent<Animator>();
        m2_Animator = level2.GetComponent<Animator>();
        m3_Animator = level3.GetComponent<Animator>();
      
    }

    // Update is called once per frame
    void Update()
    {
        Scene scene = SceneManager.GetActiveScene();
        if (scene.name == "1")
        {
            m1_Animator.SetBool("activeScene", true);
          //  m2_Animator.SetBool("activeScene", true);
            m3_Animator.SetBool("activeScene", false);
        }
          if (scene.name == "2")
        {
            m1_Animator.SetBool("activeScene", false);
          m2_Animator.SetBool("activeScene", true);
            //m3_Animator.SetBool("activeScene", false);
        }
        if (scene.name == "3")
        {
           // m1_Animator.SetBool("activeScene", false);
             m2_Animator.SetBool("activeScene", false);
            m3_Animator.SetBool("activeScene", true);
        }

        DontDestroyOnLoad(timeline);

    }
}
